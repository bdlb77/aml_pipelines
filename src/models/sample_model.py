"""A Sample Model
"""

def sample_model() -> str:
    """I am a sample model

    Returns:
        str: returns that I am a model
    """

    return "I am a model!"
