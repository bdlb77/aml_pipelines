"""
Contains helper functions for the common package
"""
import re


def sanitize_non_alphabet(name: str):
    """
    sanitize_non_alphabet removes non-alphanumeric characters from a name.

    Parameters:
        name (str): String name to be sanitized

    Returns:
        str: Sanitized name
    """
    return re.sub("[^a-zA-Z1-9]+", "", name)
