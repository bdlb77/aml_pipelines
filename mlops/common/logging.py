"""
    log_group.py defined functions for log groups within code.
"""
import traceback
from datetime import datetime
from logging import Logger

from azureml.core import Run


class MetricLogGroup():
    """
        MetricLogGroup is used for grouping code in indented blocks and
        printing log messages when the group is entered and exited.

        Parameters:
            message (str): The message to post on enter and exit of the group.
            logger (Logger): The logger to use for the Log Group
    """
    _start_time: datetime

    def __init__(self, message: str, logger: Logger):
        self._logger = logger
        self._message = message

    def __enter__(self):
        self._start_time = datetime.utcnow()
        self._logger.info(f'Start - {self._message}')

    def __exit__(self, *_, trace_back):
        if trace_back is not None:
            self._logger.error(f'Error - {self._message}')
            self._logger.error(''.join(traceback.format_tb(trace_back)))
        else:
            duration = datetime.utcnow() - self._start_time
            Run.get_context().log(f"{self._message} Duration", duration.seconds)
            self._logger.info(f'End - {self._message}')


class LogGroup():
    """
        LogGroup is used for grouping code in indented blocks and printing
        log messages when the group is entered and exited.

        Parameters:
            message (str): The message to post on enter and exit of the group.
            logger (Logger): The logger to use for the Log Group
    """
    def __init__(self, message: str, logger: Logger):
        self._logger = logger
        self._message = message

    def __enter__(self):
        self._logger.info(f'Start - {self._message}')

    def __exit__(self, _1, _2, trace_back):
        if trace_back is not None:
            self._logger.error(f'Error - {self._message}')
            self._logger.error(''.join(traceback.format_tb(trace_back)))
        else:
            self._logger.info(f'End - {self._message}')
