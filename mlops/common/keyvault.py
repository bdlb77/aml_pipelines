"""
Keyvault interaction for AzureML pipelines
"""
from logging import getLogger

from azureml.core.experiment import Experiment
from azureml.core.keyvault import Keyvault
from azureml.core.run import Run, _OfflineRun, _SubmittedRun
from azureml.core.workspace import Workspace
from mlops.common.config import WorkspaceConfig
from mlops.common.workspace import get_workspace

log = getLogger(__name__)


def get_workspace_keyvault(workspace_config: WorkspaceConfig = None) -> Keyvault:
    """
    Gets the workspace keyvault in offline or online mode

    Parameters:
        workspace_config (WorkspaceConfig): Provide workspace config when
            running in offline mode to get the Keyvault instance.

    Returns:
        Keyvault instance

    Raises:
        TypeError: if the run is neither an _OfflineRun or _SubmittedRun
    """
    run_context = Run.get_context(allow_offline=True)
    if isinstance(run_context, _OfflineRun):
        log.debug("Get workspace offline mode")
        workspace = get_workspace(
            workspace_config.name,
            workspace_config.resource_group,
            workspace_config.subscription_id,
            workspace_config.tenant_id,
            workspace_config.credentials.app_id,
            workspace_config.credentials.app_secret,
            workspace_config.region,
        )

        log.info("Get secrets in offline mode")
    elif isinstance(run_context, _SubmittedRun):
        log.info("Get secrets in online mode")
        experiment: Experiment = run_context.experiment
        workspace: Workspace = experiment.workspace
    else:
        raise TypeError('Run context is neither an offline run nor a submitted run')

    return workspace.get_default_keyvault()
