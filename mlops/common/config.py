"""
config.py loads structured configuration using vyper-config.
The configuration can be set via the accompanying yaml file and overriden by setting the upper snake case of
the corresponding value as an environment variable.

For example:
    The value workspace.tenantId can be overriden by setting WORKSPACE_TENANTID
"""
from dataclasses import MISSING, dataclass
from typing import Dict, List

from vyper import v

from mlops.common.helpers import sanitize_non_alphabet

MODEL_BASE_NAME = "model"
PIPELINE_BASE_NAME = "pipe"
EXPERIMENT_BASE_NAME = "exp"
ITERATION_BASE_NAME = "iteration"


@dataclass
class CredentialsConfig:
    """
    CredentialsConfig stores configuration for the AzureML workspace service principal
    """

    app_id: str = MISSING
    app_secret: str = MISSING


@dataclass
class VirtualNetworkConfig:
    """
    VirtualNetworkConfig stores configuration for the AzureML compute cluster virtual network
    """

    # Azure ML compute cluster virtual network name
    name: str = MISSING
    # Azure ML compute cluster virtual network resource group
    resource_group: str = MISSING
    # Azure ML compute cluster virtual network subnet name
    subnet_name: str = MISSING


@dataclass
class ComputeConfig:
    """
    ComputeConfig stores configuration for the AzureML compute cluster
    """

    # Azure ML compute cluster virtual network
    virtual_network: VirtualNetworkConfig = MISSING

    # Azure ML compute cluster name
    name: str = MISSING
    # Azure ML compute cluster sku
    size: str = "Standard_DS3_v2"
    # Azure ML compute cluster priority
    priority: str = "lowpriority"
    # Azure ML compute cluster min nodes
    min_nodes: int = 0
    # Azure ML compute cluster max nodes
    max_nodes: int = 4
    # Azure ML compute cluster scale down timeout
    scale_down_timeout: int = 600


@dataclass
class StorageConfig:
    """
    StorageConfig stores configuration for the AzureML experiment storage account
    """

    # Experiment storage account name
    name: str = MISSING
    # Experiment storage account container name
    container: str = MISSING
    # Experiment storage account key
    key: str = MISSING


@dataclass
class WorkspaceConfig:
    """
    WorkspaceConfig stores configuration for the AzureML workspace
    """

    # Azure ML Workspace tenant ID
    tenant_id: str = MISSING
    # Azure ML Workspace subscription ID
    subscription_id: str = MISSING
    # Azure ML Workspace location
    region: str = MISSING
    # Azure ML Workspace resource group
    resource_group: str = MISSING
    # Azure ML Workspace name
    name: str = MISSING
    # Azure ML Workspace credentials
    credentials: CredentialsConfig = MISSING
    # Azure ML default docker image
    docker_image: str = MISSING

    compute: Dict[str, ComputeConfig] = MISSING
    storage: StorageConfig = MISSING

    def __init__(self) -> None:
        self.tenant_id = v.get_string("workspace.tenantId")
        self.subscription_id = v.get_string("workspace.subscriptionId")
        self.region = v.get_string("workspace.region")
        self.resource_group = v.get_string("workspace.resourceGroup")
        self.name = v.get_string("workspace.name")
        self.docker_image = v.get_string("workspace.dockerImage")

        self.credentials = CredentialsConfig(
            app_id=v.get_string("workspace.credentials.appId"),
            app_secret=v.get_string("workspace.credentials.appSecret"),
        )

        self.compute = {}
        compute = v.get("workspace.compute")
        if compute:
            for key, val in compute.items():
                virtual_network_val = val["virtualNetwork"]
                virtual_network = VirtualNetworkConfig(
                    name=virtual_network_val["name"],
                    resource_group=virtual_network_val["resourceGroup"],
                    subnet_name=virtual_network_val["subnetName"],
                )

                # Compute cluster names must be between 2 and 16 characters
                assert len(val["name"]) >= 2 and len(val["name"]) <= 16

                self.compute[key] = ComputeConfig(
                    name=val["name"],
                    size=val["size"],
                    priority=val["priority"],
                    min_nodes=val["minNodes"],
                    max_nodes=val["maxNodes"],
                    scale_down_timeout=val["scaleDownTimeout"],
                    virtual_network=virtual_network,
                )

        self.storage = StorageConfig(
            name=v.get_string("workspace.storage.name"),
            container=v.get_string("workspace.storage.container"),
            key=v.get_string("workspace.storage.key"),
        )


################################################################################
# Pipeline Configuration                                                       #
################################################################################
@dataclass
class PipelineConfig:
    """
    PipelineConfig defines base properties for the other pipeline configurations.
    """

    # The name of the experiment
    experiment_name: str = MISSING
    # The name of the model to use
    model_name: str = MISSING
    # The name of the pipeline
    name: str = MISSING
    # The name of the iteration
    iteration_name: str = MISSING

    blob_datastore_name: str = MISSING
    build_id: str = MISSING

    def __post_init__(self, pipeline_type: str) -> None:
        # Determine CI framework to set branch_id and branch_name
        ci_framework = v.get("CI_FRAMEWORK")
        build_and_branch_names = resolve_branch_name(ci_framework)
        self.build_id = build_and_branch_names[0]
        branch_name = build_and_branch_names[1]
        # Experiment names cannot contain non-alphanumeric characters
        sanitized_branch_name = sanitize_non_alphabet(branch_name)

        self.name = f"{pipeline_type}_{PIPELINE_BASE_NAME}_{sanitized_branch_name}"
        self.model_name = f"{pipeline_type}_{MODEL_BASE_NAME}_{sanitized_branch_name}"
        self.experiment_name = (f"{pipeline_type}_{EXPERIMENT_BASE_NAME}_{sanitized_branch_name}")
        self.iteration_name = f"{pipeline_type}_{ITERATION_BASE_NAME}_{self.build_id}"


@dataclass
class SampleTrainingPipelineConfig(PipelineConfig):
    """
    TrainingPipelineConfig stores configuration for the training pipeline.
    """
    def __init__(self) -> None:
        super().__post_init__("training")

        self.blob_datastore_name = v.get_string("pipelines.training.blobDatastoreName")


def resolve_branch_name(ci_framework: str) -> List[str]:
    """Resolve the branch name for CI Framework

    Parameters:
        ci_framework (str): CI Framework used. Ex. Gitlab, Azure.

    Returns:
        List[str, str]: [build_id, branch_name]

    Raises:
        Exception: CI_FRAMEWORK environment variable not defined
    """
    build_id = ""
    branch_name = ""
    if not ci_framework or ci_framework == "":
        raise Exception("Unable to Instantiate PipelineConfig due to CI_FRAMEWORK not defined.")
    elif ci_framework == "azure":
        build_id = v.get("build.buildId")
        branch_name = v.get_string("build.sourceBranchName")
    elif ci_framework == "gitlab":
        build_id = v.get("ci.pipeline.id")
        branch_name = v.get("ci.commit.branch")
        # ci.commit.branch not available in Merge
        if not branch_name and v.get("CI_MERGE_REQUEST_SOURCE_BRANCH_NAME"):
            branch_name = v.get("CI_MERGE_REQUEST_SOURCE_BRANCH_NAME")
    return [build_id, branch_name]


@dataclass
class DataPrepPipelineConfig(PipelineConfig):
    """
    DataPrepPipelineConfig stores configuration for the data prep pipeline.
    """
    def __init__(self) -> None:
        super().__post_init__("data_prep")

        self.blob_datastore_name = v.get_string("pipelines.dataprep.blobDatastoreName")
        self.blob_datastore_path = v.get_string("pipelines.dataprep.blobDatastorePath")
        self.dataset_name = v.get_string("pipelines.dataprep.prepDatasetName")
