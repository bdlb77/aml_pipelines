"""
Tests for common helpers
"""
import pytest

from mlops.common.helpers import sanitize_non_alphabet

test_names = [
    ("branch/hello-world", "branchhelloworld"),
    ("branch\\hello-world", "branchhelloworld"),
    ("branch/hello-world123", "branchhelloworld"),
    ("branch/#74-hello-world123", "branchhelloworld"),
]


@pytest.mark.parametrize("name,expected", test_names)
def test_cv_project_init(name: str, expected: str):
    """
    Tests sanitizing strings to alphanumeric.

    Parameters:
        name (str): A string with non-alphanumeric letters
        expected (str): The expected output of the sanitization
    """
    output_name = sanitize_non_alphabet(name)
    assert output_name == expected
