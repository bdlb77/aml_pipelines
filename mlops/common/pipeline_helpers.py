"""
This is a helper for all Azure ML Pipelines
"""
import os
import sys
from logging import getLogger

from azureml.core.runconfig import Environment
from azureml.pipeline.core import Pipeline, PublishedPipeline  # type: ignore

from mlops.common.attach_compute import get_compute
from mlops.common.config import WorkspaceConfig
from mlops.common.workspace import get_workspace

log = getLogger(__name__)


def pipeline_base(workspace: WorkspaceConfig):
    """
    Gets AzureML artifacts: AzureML Workspace, AzureML Compute Tagret and AzureMl Run Config
    Parameters:
        workspace (WorkspaceConfig): configuration for the Azure ML workspace
    Returns:
        Workspace: a reference to the current workspace
        ComputeTarget: compute cluster object
        Environment: environment for compute instances
    """
    # Get Azure machine learning workspace
    aml_workspace = get_workspace(
        workspace.name,
        workspace.resource_group,
        workspace.subscription_id,
        workspace.tenant_id,
        workspace.credentials.app_id,
        workspace.credentials.app_secret,
        workspace.region,
        create_if_not_exist=False,
    )
    log.info("Got workspace: %s", aml_workspace.name)

    compute = workspace.compute["amlCompute"]

    # Get Azure machine learning cluster
    aml_compute = get_compute(
        aml_workspace,
        compute.name,
        compute.size,
        compute.priority,
        compute.min_nodes,
        compute.max_nodes,
        compute.scale_down_timeout,
        compute.virtual_network.name,
        compute.virtual_network.subnet_name,
        compute.virtual_network.resource_group,
    )

    if aml_compute is not None:
        log.info("Using compute: %s", aml_compute.name)
        base_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        sys.path.append(base_path)
        batch_env = Environment.from_conda_specification("train-env", "./conda-env.yml")
        batch_env.docker.enabled = True
        batch_env.docker.base_image = workspace.docker_image

    return aml_workspace, aml_compute, batch_env


def publish_pipeline(aml_workspace, steps, pipeline_name, build_id) -> PublishedPipeline:
    """
    Publishes a pipeline to the AzureML Workspace
    Parameters:
      aml_workspace (Workspace): existing AzureML Workspace object
      steps (list): list of PipelineSteps
      pipeline_name (string): name of the pipeline to be published
      build_id (string): DevOps Pipeline Build Id

    Returns:
        PublishedPipeline
    """
    train_pipeline = Pipeline(workspace=aml_workspace, steps=steps)
    train_pipeline.validate()
    published_pipeline = train_pipeline.publish(
        name=pipeline_name,
        description="Model training/retraining pipeline",
        version=build_id,
    )
    log.info('Published pipeline: %s for build: %s', published_pipeline.name, build_id)

    return published_pipeline
