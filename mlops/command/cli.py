"""
The main entrypoint for the AML pipeline CLI
"""
from mlops.command.helpers.pipeline_cli import PipelineCLI, run
from mlops.command.pipelines.sample_training_pipeline import \
    SampleTrainingPipelineGroup

cli = PipelineCLI()
# Register pipelines
SampleTrainingPipelineGroup(cli)

# Pylint complains that there is no parameter given for this method
# because it expects a config parameter, however click injects this parameter.
# pylint: disable=no-value-for-parameter
run()
