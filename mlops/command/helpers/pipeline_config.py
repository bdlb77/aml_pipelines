"""Initialize the config.yaml variables into the environment
    """
import pathlib
from logging import getLogger, basicConfig, INFO
from logging.config import dictConfig
from vyper import v


def load_config(config_path: str):
    """
    load_config loads the configuration file.

    Parameters:
        config_path (str): Path for the config file
    """
    basicConfig(
        level=INFO,
        format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    )
    log = getLogger(__name__)

    log.debug("Use environment variables for configuration")
    v.automatic_env()

    log.debug("Use sample config file as the default configuration")
    v.set_config_file("config.base.yaml")
    v.read_in_config()

    log.debug("Parse given config file path")
    conf_path = pathlib.Path(config_path)
    file_name = conf_path.stem
    file_path = str(conf_path.parent)

    log.debug("Use given config file as configuration")
    v.add_config_path(file_path)
    v.set_config_name(file_name)
    v.set_config_type("yaml")

    try:
        log.debug("Merge both configuration files")
        v.merge_in_config()
    except FileNotFoundError as ex:
        log.warning("Could not find config file %s: %s", config_path, ex)

    log_config = v.get("logging")
    ignore_info_logger = {'level': 'WARNING', 'handlers': ['console', 'error']}

    log.debug("Ignore library info levels")
    log_config['loggers']['azureml'] = ignore_info_logger

    dictConfig(log_config)
