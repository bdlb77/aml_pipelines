"""Creates helper structures for creating the pipeline CLI"""
import click

from mlops.command.helpers.pipeline_config import load_config


@click.group()
@click.option("--config", default="config.yaml")
# Disable linter rules:
#   unused-variable: which complains this method is unused
#   missing-param-doc: click outputs the docstring as part of the help message in the CLI
# pylint: disable=missing-param-doc
def run(config: str):
    """Load the config and run the pipelines CLI"""
    load_config(config)


class PipelineCLI(click.Group):
    """
    This class sets up the command hierarchy which will get injected into each pipeline.
    Each pipeline will add its own subcommands onto these commands.
    """
    @staticmethod
    @run.group()
    def get_info():
        """Gets information for a published pipeline"""

    @staticmethod
    @run.group()
    def publish():
        """Publishes a pipeline"""

    @staticmethod
    @run.group()
    def run():
        """Runs a pipeline"""
