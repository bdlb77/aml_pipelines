"""
The base class for each pipeline command implementation.

Each pipeline will inherit from this class and implement its own methods for get_info, publish and run.
"""
from typing import Any, Dict

from azureml.core.workspace import Workspace
from azureml.pipeline.core.graph import PublishedPipeline
from azureml.pipeline.core.run import PipelineRun
from mlops.common.config import WorkspaceConfig
from mlops.common.get_pipeline import \
    get_pipeline as get_published_pipeline
from mlops.common.workspace import get_workspace


class PipelineCommandBase:
    """
    PipelineCommandBase is the base class for implementing pipelines.

    This class contains helper functions for interacting with Azure ML pipeline APIs.
    """
    @staticmethod
    def get_workspace() -> Workspace:
        """
        Get the workspace context from the WorkspaceConfig

        Returns:
            workspace (Workspace): the workspace associated with this pipeline.
        """
        ws_config = WorkspaceConfig()
        workspace = get_workspace(
            ws_config.name,
            ws_config.resource_group,
            ws_config.subscription_id,
            ws_config.tenant_id,
            ws_config.credentials.app_id,
            ws_config.credentials.app_secret,
            ws_config.region,
            create_if_not_exist=False,
        )

        return workspace

    @staticmethod
    def get_published_pipeline(name: str, build_id: str) -> PublishedPipeline:
        """
        Get the published pipeline for this branch from the workspace.

        Parameters:
            name (str): Name of the pipeline
            build_id (str): The current build ID

        Returns:
            pipeline (PublishedPipeline): the published pipeline for this branch.
        """
        workspace = PipelineCommandBase.get_workspace()
        pipeline = get_published_pipeline(
            workspace,
            name,
            build_id,
        )

        return pipeline

    @staticmethod
    def submit_experiment(
        pipeline_name: str,
        pipeline_build_id: str,
        experiment_name: str,
        parameters: Dict[str, Any],
    ) -> PipelineRun:
        """
        Submit an experiment to the published pipeline.

        Parameters:
            pipeline_name (str): The name of the published pipeline.
            pipeline_build_id (str): The build ID for the published pipeline.
            experiment_name (str): The name of the experiment
            parameters (Dict[str, Any]): Parameters to use as input to the pipeline.

        Returns:
            experiment_run (PipelineRun): The experiment run submitted to the pipeline.
        """
        workspace = PipelineCommandBase.get_workspace()

        pipeline = PipelineCommandBase.get_published_pipeline(
            pipeline_name,
            pipeline_build_id,
        )

        experiment_run = pipeline.submit(
            workspace,
            experiment_name,
            pipeline_parameters=parameters,
        )

        return experiment_run
