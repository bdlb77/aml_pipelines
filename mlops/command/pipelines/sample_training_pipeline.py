"""
Register the CLI command group for the  training pipeline
"""
from logging import getLogger
from typing import Any, Dict

import click

from mlops.command.helpers.pipeline_cli import PipelineCLI
from mlops.command.helpers.pipeline_cmd_base import PipelineCommandBase
from mlops.common.config import SampleTrainingPipelineConfig, WorkspaceConfig
from mlops.common.get_datastores import get_blob_datastore
from mlops.common.pipeline_helpers import pipeline_base, publish_pipeline
from mlops.sample_training_pipeline.create_and_publish_pipeline import \
    get_pipeline_steps

log = getLogger(__name__)


class SampleTrainingPipelineGroup(PipelineCommandBase):
    """
    This class registers the CLI commands which interact with the training pipeline
    """
    name: str = "sample-training-pipeline"

    def __init__(self, cli: PipelineCLI):
        """
        Registers the commands which interact with the training pipeline
        and initializes the configuration and workspace.

        Parameters:
            cli (PipelineCLI): A helper class for adding subcommands and building the CLI hierarchy
        """
        log.info("Registering training pipeline commands")
        cli.get_info.add_command(self.get_info, self.name)
        cli.publish.add_command(self.publish, self.name)
        cli.run.add_command(self.run, self.name)

    @staticmethod
    @click.command()
    @click.option("--output_pipeline_file", default="pipeline_info.txt")
    # Disable linter rules:
    #   unused-variable: which complains this method is unused
    #   missing-param-doc: click outputs the docstring as part of the help message in the CLI
    # pylint: disable=unused-variable, missing-param-doc, missing-return-doc, missing-return-type-doc
    def get_info(output_pipeline_file: str) -> None:
        """Get information for a published training pipeline"""
        pipeline_config = SampleTrainingPipelineConfig()

        log.info("Get the published pipeline %s", pipeline_config.name)
        pipeline = PipelineCommandBase.get_published_pipeline(
            pipeline_config.name,
            pipeline_config.build_id,
        )

        if output_pipeline_file is not None:
            log.info("Write the published pipeline details to file %s", output_pipeline_file)
            with open(output_pipeline_file, "w") as out_file:
                out_file.write(pipeline.id + "\n")
                out_file.write(pipeline_config.experiment_name + "\n")
                out_file.write(pipeline.endpoint)

    @staticmethod
    @click.command()
    # Disable linter rules:
    #   unused-variable: which complains this method is unused
    #   missing-param-doc: click outputs the docstring as part of the help message in the CLI
    # pylint: disable=unused-variable, missing-param-doc, missing-return-doc, missing-return-type-doc
    def publish() -> str:
        """Publish the training pipeline"""
        ws_config = WorkspaceConfig()
        pipeline_config = SampleTrainingPipelineConfig()

        log.debug("Publish the pipeline %s", pipeline_config.name)
        aml_workspace, aml_compute, batch_env = pipeline_base(ws_config)

        log.debug(
            "Getting the blob datastore %s for %s",
            pipeline_config.blob_datastore_name,
            ws_config.storage.name,
        )
        blob_ds = get_blob_datastore(
            aml_workspace,
            pipeline_config.blob_datastore_name,
            ws_config.storage.name,
            ws_config.storage.key,
            ws_config.storage.container,
        )

        log.debug("Getting the pipeline steps")
        steps = get_pipeline_steps(
            aml_compute,
            blob_ds,
            batch_env,
            pipeline_config,
        )

        log.debug("Publish the pipeline")
        published_pipeline = publish_pipeline(
            aml_workspace,
            steps,
            pipeline_config.name,
            pipeline_config.build_id,
        )

        return published_pipeline.id

    @staticmethod
    @click.command()
    @click.option("--wait/--no-wait", default=False)
    # Pipeline Parameters
    @click.option("--input_folder", default="toy_dataset")
    # pylint: disable=unused-variable, missing-param-doc, missing-return-doc, missing-return-type-doc
    def run(wait: bool, **kwargs: Dict[str, Any]) -> str:
        """Run the training pipeline"""
        pipeline_config = SampleTrainingPipelineConfig()

        log.debug("Running the pipeline %s", pipeline_config.name)
        parameters = {
            "input_folder": kwargs["input_folder"],
        }

        log.debug("Submit the experiment %s", pipeline_config.experiment_name)
        experiment_run = PipelineCommandBase.submit_experiment(
            pipeline_config.name,
            pipeline_config.build_id,
            pipeline_config.experiment_name,
            parameters,
        )

        if wait:
            log.debug("Waiting for experiment %s completion", pipeline_config.experiment_name)
            # Handle Status from `wait_for_completion`. Raise error != succeed
            experiment_run.wait_for_completion(show_output=True)

        log.debug(
            "Submitted experiment %s with run ID %s",
            pipeline_config.experiment_name,
            experiment_run.id,
        )
        return experiment_run.id
