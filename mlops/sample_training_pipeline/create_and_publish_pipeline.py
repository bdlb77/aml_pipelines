"""
A script to create and publish the basic pipeline
We are running it as a module to solve import problems
"""
from logging import getLogger

from azureml.core import RunConfiguration
from azureml.core.compute import ComputeTarget
from azureml.core.datastore import Datastore
from azureml.core.runconfig import Environment
from azureml.data.datapath import DataPath, DataPathComputeBinding
from azureml.pipeline.core import PipelineParameter
from azureml.pipeline.steps import PythonScriptStep

from mlops.common.config import SampleTrainingPipelineConfig

log = getLogger(__name__)


def get_pipeline_steps(
    aml_compute: ComputeTarget,
    blob_ds: Datastore,
    batch_env: Environment,
    pipeline_config: SampleTrainingPipelineConfig,
) -> str:
    """
    Creates pipeline steps

    Parameters:
        aml_compute (ComputeTarget): a reference to a compute
        blob_ds (DataStore): a reference to a datastore
        batch_env (Environment): a reference to environment object
        pipeline_config (TrainingPipelineConfig): Configuration object for this pipeline
    Returns:
        string: published pipeline id
    """

    # Just an example how we can use parameters to provide different
    # input folder
    # Create dataset using blob_ds and input_folder as location in blob store

    data_path = DataPath(datastore=blob_ds, path_on_datastore="toy_dataset")
    input_folder_param = PipelineParameter("input_folder", default_value=data_path)
    data_path_input = (input_folder_param, DataPathComputeBinding(mode="mount"))

    basic_step_config = RunConfiguration()
    basic_step_config.environment = batch_env
    basic_step = PythonScriptStep(
        name="basic_step",
        script_name="mlops/sample_training_pipeline/steps/basic_step.py",
        runconfig=basic_step_config,
        arguments=[
            "--input",
            data_path_input,
            "--model_name",
            pipeline_config.model_name,
            "--build_id",
            pipeline_config.build_id,
        ],
        inputs=[data_path_input],
        compute_target=aml_compute,
        allow_reuse=False,
    )

    log.info("Pipeline Steps Created")

    steps = [basic_step]

    log.info("Returning %d steps", len(steps))
    return steps
