"""
Basic step to do a task on a compute
"""
import argparse
import logging
import os

from azureml.core import Run
from azureml.core.model import Model

# from ml.algorithms import train_fake_model


def main():
    """Main Script for running `basic_step` Script in AML TrainingWorkflow
    """
    logging.basicConfig(level=logging.INFO)
    log: logging.Logger = logging.getLogger(__name__)
    log.info("Basic step")
    parser = argparse.ArgumentParser()
    parser.add_argument("--input", type=str, required=True)
    parser.add_argument("--model_name", type=str, required=True)
    parser.add_argument("--build_id", type=str, required=True)
    args, _ = parser.parse_known_args()
    # Call any DS related code
    log.info("Training a model %s", args.model_name)
    model_content = train_fake_model(args.input)
    log.info("Training has been completed")

    # Prepare a model for registration
    os.makedirs("outputs", exist_ok=True)
    model_path = os.path.join("outputs", f"{args.model_name}.txt")

    with open(model_path, "w") as f_handler:
        f_handler.write(model_content)

    model_tags = {
        "build_id": args.build_id,
    }

    # Register the model
    run = Run.get_context()
    Model.register(
        model_path=model_path,
        model_name=args.model_name,
        tags=model_tags,
        workspace=run.experiment.workspace,
    )
    log.info("Model has been registered")


def train_fake_model(input_data: str):
    """the function to train a fake model

    Parameters:
        input_data (str): Input Data to train Fake model

    Returns:
        str: Return the string, "I am a model"
    """
    print(f"Using data from here: {input_data}")
    return "I am a model"


if __name__ == "__main__":
    main()
