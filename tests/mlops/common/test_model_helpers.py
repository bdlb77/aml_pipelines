"""This Module tests Model Helpers
    """
# from unittest.mock import Mock, patch
import json
import os
import tempfile

from mlops.common.model_helpers import write_model_info


def test_write_model_info():
    """test write model info function
    """
    folder_path = tempfile.mkdtemp()
    output_file = tempfile.mkstemp()[1]
    model_info = {"project_name": "name", "project_id": "12345"}
    try:
        write_model_info(folder_path, output_file, model_info)
        file_path = os.path.join(folder_path, output_file)
        contents = open(file_path).read()
    finally:
        os.remove(file_path)
    content_dict = json.loads(contents)
    assert content_dict == model_info
