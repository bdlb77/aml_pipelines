"""Sample Test Module
"""
from src.models.sample_model import sample_model

def test_sample_model():
    """I am a Sample model Test
    """
    model_name = "I am a model!"
    assert model_name == sample_model()
